from pprint import pprint
from PyInquirer import style_from_dict, Token, prompt, Separator
from time import sleep
import os

custom_style = style_from_dict({
    Token.Separator: '#66FF00',
    Token.QuestionMark: '#FF9D00',
    Token.Selected: '#FF0000',
    Token.Pointer: '#FF9D00',
    Token.Instruction: '',
    Token.Answer: '#5F819D bold',
    Token.Question: '',
})

questions = [
    {
        'type': 'checkbox',
        'message': 'Select Softwares',
        'name': 'softwares',
        'choices': [
            Separator('### The Developer Tools ###'),
            {
                'name': 'Git'
            },
            {
                'name': 'Pycharm'
            },
            {
                'name': 'Visual Studio'
            },
            {
                'name': 'Eclipse'
            },
            Separator('### Developer Kits ###'),
            {
                'name': 'Java Development Kit'
            },
            Separator('### File Management ###'),
            {
                'name': 'WinRar'
            },
            {
                'name': '7zip'
            },
            Separator('### Browser ###'),
            {
                'name': 'Chrome'
            },
            {
                'name': 'Firefox'
            },
            Separator('### Entertainment ###'),
            {
                'name': 'Steam'
            },
            {
                'name': 'Battle Net'
            },
            {
                'name': 'Origin'
            },
            {
                'name': 'Uplay'
            },
            Separator('### Utility ###'),
            {
                'name': 'Discord'
            },
            {
                'name': 'Team Speak'
            },
        ],
    }
]

answers = prompt(questions, style=custom_style)

for dict in answers.values():
    for item in dict:
        if item is "Git":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/developer_tools/git.bat")
        if item is "Pycharm":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/developer_tools/pycharm.bat")
        if item is "Visual Studio":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/developer_tools/visual_studio.bat")
        if item is "Eclipse":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/developer_tools/eclipse.bat")
        if item is "Java Development Kit":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/developer_kits/jdk.bat")
        if item is "WinRar":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/file_management/winrar.bat")
        if item is "7zip":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/file_management/7zip.bat")
        if item is "Chrome":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/browser/chrome.bat")
        if item is "Firefox":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/browser/firefox.bat")
        if item is "Steam":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/entertainment/steam.bat")
        if item is "Battle Net":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/entertainment/battlenet.bat")
        if item is "Origin":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/entertainment/origin.bat")
        if item is "Uplay":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/entertainment/uplay.bat")
        if item is "Discord":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/utility/discord.bat")
        if item is "Team Speak":
            os.system(os.path.dirname(os.path.realpath(__file__)) + "/batch_shell_scripts/utility/teamspeak.bat")

print("The process has finished")
input()