function RefreshEnvPath
{
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") `
        + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
RefreshEnvPath

choco install python --yes
RefreshEnvPath

choco install pip --yes
RefreshEnvPath

pip install PyInquirer

cd $PSScriptRoot
python main_python.py

Read-Host -Prompt "The process has finished press any button to exit."
